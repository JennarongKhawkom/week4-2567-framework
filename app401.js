const days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
const weeks = ["one","two","three","four","five","six","seven"]
//Destructuring assignment
const [day1,day3,day7] = days
console.log(day1,day3,day7)

const [weel1, week2, args] = weeks
console.log(week1)
console.log(week2)
console.log(args);